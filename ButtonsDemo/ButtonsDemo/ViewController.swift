//
//  ViewController.swift
//  ButtonsDemo
//
//  Created by Miha Hozjan on 13/12/2018.
//  Copyright © 2018 Connected Travel, LLC. All rights reserved.
//

import UIKit

fileprivate func imageWithColor(color: UIColor) -> UIImage {
    let rect: CGRect = CGRect(x: 0, y: 0, width: 1, height: 1)
    UIGraphicsBeginImageContextWithOptions(CGSize(width: 1, height: 1), false, 0)
    color.setFill()
    UIRectFill(rect)
    let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
    UIGraphicsEndImageContext()
    return image
}

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let item = UIBarButtonItem(title: "Click me", style: .done, target: self, action: #selector(itemPressed(_:)))
        item.setBackgroundImage(imageWithColor(color: .yellow), for: .normal, style: .done, barMetrics: .default)
        navigationItem.rightBarButtonItem = item
    }

    @IBAction func buttonPressed(_ sender: Any) {
        print("Clicked")
        showAlert()
    }
    
    // MARK: - UIBarButtonItem
    
    @objc func itemPressed(_ sender: Any) {
        print("Clicked")
        showAlert()
    }
    
    // MARK: Helpers
    
    func showAlert() {
        let vc = UIAlertController(title: "Clicked!", message: nil, preferredStyle: .alert)
        vc.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(vc, animated: true, completion: nil)
    }
}

